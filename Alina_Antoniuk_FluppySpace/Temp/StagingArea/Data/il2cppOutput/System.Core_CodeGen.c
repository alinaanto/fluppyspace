﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000003 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000004 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000007 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000009 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000000A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000000B System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000000C System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000000D System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000010 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000011 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000013 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000014 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000016 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000018 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000019 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000001C System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000001D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000001F System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
static Il2CppMethodPointer s_methodPointers[31] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[31] = 
{
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000004, { 22, 4 } },
	{ 0x02000005, { 26, 9 } },
	{ 0x02000006, { 35, 7 } },
	{ 0x02000007, { 42, 10 } },
	{ 0x02000008, { 52, 1 } },
	{ 0x06000003, { 0, 10 } },
	{ 0x06000004, { 10, 5 } },
	{ 0x06000005, { 15, 3 } },
	{ 0x06000006, { 18, 1 } },
	{ 0x06000007, { 19, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[53] = 
{
	{ (Il2CppRGCTXDataType)2, 8268 },
	{ (Il2CppRGCTXDataType)3, 3443 },
	{ (Il2CppRGCTXDataType)2, 8269 },
	{ (Il2CppRGCTXDataType)2, 8270 },
	{ (Il2CppRGCTXDataType)3, 3444 },
	{ (Il2CppRGCTXDataType)2, 8271 },
	{ (Il2CppRGCTXDataType)2, 8272 },
	{ (Il2CppRGCTXDataType)3, 3445 },
	{ (Il2CppRGCTXDataType)2, 8273 },
	{ (Il2CppRGCTXDataType)3, 3446 },
	{ (Il2CppRGCTXDataType)2, 8274 },
	{ (Il2CppRGCTXDataType)3, 3447 },
	{ (Il2CppRGCTXDataType)3, 3448 },
	{ (Il2CppRGCTXDataType)2, 7054 },
	{ (Il2CppRGCTXDataType)3, 3449 },
	{ (Il2CppRGCTXDataType)2, 7056 },
	{ (Il2CppRGCTXDataType)2, 8275 },
	{ (Il2CppRGCTXDataType)3, 3450 },
	{ (Il2CppRGCTXDataType)2, 7059 },
	{ (Il2CppRGCTXDataType)2, 7061 },
	{ (Il2CppRGCTXDataType)2, 8276 },
	{ (Il2CppRGCTXDataType)3, 3451 },
	{ (Il2CppRGCTXDataType)3, 3452 },
	{ (Il2CppRGCTXDataType)3, 3453 },
	{ (Il2CppRGCTXDataType)2, 7066 },
	{ (Il2CppRGCTXDataType)3, 3454 },
	{ (Il2CppRGCTXDataType)3, 3455 },
	{ (Il2CppRGCTXDataType)2, 7075 },
	{ (Il2CppRGCTXDataType)2, 8277 },
	{ (Il2CppRGCTXDataType)3, 3456 },
	{ (Il2CppRGCTXDataType)3, 3457 },
	{ (Il2CppRGCTXDataType)2, 7077 },
	{ (Il2CppRGCTXDataType)2, 8203 },
	{ (Il2CppRGCTXDataType)3, 3458 },
	{ (Il2CppRGCTXDataType)3, 3459 },
	{ (Il2CppRGCTXDataType)3, 3460 },
	{ (Il2CppRGCTXDataType)2, 7084 },
	{ (Il2CppRGCTXDataType)2, 8278 },
	{ (Il2CppRGCTXDataType)3, 3461 },
	{ (Il2CppRGCTXDataType)3, 3462 },
	{ (Il2CppRGCTXDataType)3, 3163 },
	{ (Il2CppRGCTXDataType)3, 3463 },
	{ (Il2CppRGCTXDataType)3, 3464 },
	{ (Il2CppRGCTXDataType)2, 7093 },
	{ (Il2CppRGCTXDataType)2, 8279 },
	{ (Il2CppRGCTXDataType)3, 3465 },
	{ (Il2CppRGCTXDataType)3, 3466 },
	{ (Il2CppRGCTXDataType)3, 3467 },
	{ (Il2CppRGCTXDataType)3, 3468 },
	{ (Il2CppRGCTXDataType)3, 3469 },
	{ (Il2CppRGCTXDataType)3, 3168 },
	{ (Il2CppRGCTXDataType)3, 3470 },
	{ (Il2CppRGCTXDataType)3, 3471 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	31,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	53,
	s_rgctxValues,
	NULL,
};
