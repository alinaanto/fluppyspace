﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ColumnPool : MonoBehaviour
{
    private int arrSize = 5;
    private GameObject[] columnArr;
    public GameObject columnPrefab;
    private Vector2 columnPos = new Vector2(-20f, -20f);//initial position of columns to spawn from

    public float respawnRate = 1f;//defines when it's time to respawn columns
    public float timerBeforeRespawn = 0f;

    public float min = -1.5f;//borders of random spawn of columns
    public float max = 3f;


    private int respawnCol = 0;


    void Start()
    {
        columnArr = new GameObject[arrSize];
        for (int i = 0; i < arrSize; i++)
        {
            columnArr[i] = (GameObject)Instantiate(columnPrefab, columnPos, Quaternion.identity);//Instantiate columns
        }
        
    }

    // Update is called once per frame
    void Update()
    {
       
        timerBeforeRespawn += Time.deltaTime;//update timer
        if (timerBeforeRespawn > respawnRate)
        {
            timerBeforeRespawn = 0f;//set a new timer

            float y_Pos = Random.Range(min, max);//set position  
            float x_Pos = Random.Range(2, 6);


            //for (int i = 0; i < arrSize; i++)//transform the instantiated array of columns
            //{
            //    

            //    columnArr[i].transform.position = new Vector2((i*2.7f) + x_Pos, y_Pos);
            //    //Debug.Log(distBetweenColumns + x_Pos);

            //}

            columnArr[respawnCol].transform.position = new Vector2(x_Pos, y_Pos);
            columnArr[respawnCol++].transform.position = new Vector2(x_Pos+2, y_Pos);

            respawnCol++;

            if (respawnCol >= arrSize)//...and start again
            {
                respawnCol = 0;
            }

        }

    }



}
